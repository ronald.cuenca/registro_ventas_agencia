/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.Agencia;

/**
 *
 * @author Ronald Cuenca
 */
public class Agencia {
    private Integer NAgencia;
    private Double venta;
    private String mes;

    public Double getVenta() {
        return venta;
    }

    public void setVenta(Double venta) {
        this.venta = venta;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }
    
    public Integer getNAgencia() {
        return NAgencia;
    }

    public void setNAgencia(Integer NAgencia) {
        this.NAgencia = NAgencia;
    }
}

