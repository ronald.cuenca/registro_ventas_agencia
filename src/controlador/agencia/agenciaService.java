/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.agencia;

import controlador.agencia.Excepcion.PosicionException;
import java.util.Scanner;
import modelo.Agencia.Agencia;

/**
 *
 * @author Ronald Cuenca
 */
public class agenciaService {
    ControladorAgencia agencia;

    public agenciaService() {
        this.agencia = new ControladorAgencia();
    }

    public ControladorAgencia getAgencia() {
        return agencia;
    }

    public void setAgencia(ControladorAgencia agencia) {
        this.agencia = agencia;
    }
     
    public boolean agregarVenta(Double cantidad){
        try {
            agencia.add(cantidad);
            return true;
        } catch (PosicionException e) {
            System.out.println(e);
        }
        return false;
    }
    
    public Double VentasAnual(Integer nAgencia){
        return agencia.ventasAnual(nAgencia);
    }
    
    public Double PromedioVentaMes(Integer Nmes){
        return agencia.promedioVentaMes(Nmes);
    }
    
    public int AgenciaMesVenta(Integer Nmes){
        return agencia.mesVentaAgencia(Nmes);
    }
    
    public int MesMenorVenta(){
        return agencia.mesMenorVentas();
    }
    
    public Agencia obtenerDato(Integer fila, Integer columna){
        try {
            return agencia.obtenerDato(fila , columna);
        } catch (PosicionException e) {
            return null;
        }
    }
    
    public Integer getLongitud(){
        return agencia.getContador();
    }
    
    public void reiniciarTabla(){
         agencia.reiniciarTabla();
    }
    
    public void guardarJson(Agencia[][]agenc){
        agencia.guardarJson(agenc);
    }
}
