/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.agencia;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import controlador.agencia.Excepcion.PosicionException;
import java.io.File;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
import modelo.Agencia.Agencia;

/**
 *
 * @author Ronald Cuenca
 */
public class ControladorAgencia extends Agencia{

    private Agencia[][] agencia;
    private Integer contadorFila;
    private Integer contadorColumna;
    private Integer cont;

    public ControladorAgencia() {
        this.agencia = new Agencia[5][12];
        contadorFila=0;
        contadorColumna=0;
        cont=0;
    }

    public Agencia[][] getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia[][] agencia) {
        this.agencia = agencia;
    }
    
    public Integer getContador(){
        return cont;
    }
    
    public void add(Double Agencia) throws PosicionException {
        if (contadorFila <= 4 && contadorFila >=0) {
            String[] mes={"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
            Agencia aux = new Agencia();
            aux.setVenta(Agencia);
            int c = contadorFila;
            aux.setNAgencia(c+1);
            aux.setMes(mes[contadorColumna]);
            Agencia[][] aux2 = agencia;
                aux2[contadorFila][contadorColumna] = (aux);
            contadorColumna++;
            if (contadorColumna==12) {
                contadorFila++;
                contadorColumna=0;
            }
            if (contadorFila==5) {
                contadorFila=0;
            }
            if (contadorFila==1) {
                cont++;
            }
        } else {
            throw new PosicionException("No existe la posicion insertada");
        }
    }

    public double ventasAnual(Integer agenc) {
        Agencia[][] aux = agencia;
        double ventasAnio = 0;
        for (int i = 0; i < 12; i++) {
            ventasAnio = (Double.parseDouble(aux[agenc][i].getVenta().toString()) + ventasAnio);
        }
        return ventasAnio;
    }

    public double promedioVentaMes(Integer mes) {
        Agencia[][] aux = agencia;
        double promedioVenta = 0;
        for (int i = 0; i < 5; i++) {
            promedioVenta = (Double.parseDouble(aux[i][mes].getVenta().toString()) + promedioVenta);
        }
        return promedioVenta / 5;
    }

    public int mesVentaAgencia(Integer mes) {
        Agencia[][] aux = agencia;
        int n = 0;
        double mayor = Double.parseDouble(aux[0][mes].getVenta().toString());
        for (int i = 0; i < 5; i++) {
            if (mayor < (Double.parseDouble(aux[i][mes].getVenta().toString()))) {
                mayor = Double.parseDouble(aux[i][mes].getVenta().toString());
                n = i;
            }
        }
        return n+1;
    }

    public int mesMenorVentas() {
        int n = 0;
        double ventaMes = promedioVentaMes(0);;
        for (int i = 0; i < 11; i++) {
            if (ventaMes > promedioVentaMes(i)) {
                ventaMes = promedioVentaMes(i);
                n = i;
            }
        }
        return n;
    }

    public Agencia obtenerDato(Integer fila, Integer columna) throws PosicionException {
        if (!(agencia==null)) {
        if (fila >= 0 && fila <= 4) {
            if (columna >= 0 && columna <= 11) {
                Agencia[][] aux2 = agencia;
                Agencia aux = null;
                if (aux2[fila][columna]==null) {
                    columna++;
                }
                aux = aux2[fila][columna];
                return aux;
            }else{
                throw new PosicionException("No existe la posicion insertada");
            }
        }else{
            throw new PosicionException("No existe la posicion insertada");
        }
        }else{
            throw new PosicionException("No se ha ingresado ningun dato, arreglo ");
        }
    }
    
    public void reiniciarTabla(){
        for (int i = 0; i <= 4; i++) {
            for (int j = 0; j <= 11; j++) {
                agencia[i][j]=null;
            }
        }
        cont=0;
        contadorColumna=0;
        contadorFila=0;
    }
    
    public void guardarJson(Agencia[][] agenc){
        Agencia[][]aux = agencia;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(aux);
        System.out.println(jsonString);
        try (PrintWriter archivo = new PrintWriter(new File("Datos_Venta.json"))){
            archivo.write(jsonString);
            archivo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        JOptionPane.showMessageDialog(null, "Datos Guardados");
    }
}
