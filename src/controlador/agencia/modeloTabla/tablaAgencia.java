/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.agencia.modeloTabla;

import controlador.agencia.ControladorAgencia;
import controlador.agencia.Excepcion.PosicionException;
import controlador.agencia.agenciaService;
import java.util.Scanner;
import javax.swing.table.AbstractTableModel;
import modelo.Agencia.Agencia;

/**
 *
 * @author Ronald Cuenca
 */
public class tablaAgencia extends AbstractTableModel{
    agenciaService agencia;

    public agenciaService getAgencia() {
        return agencia;
    }

    public void setAgencia(agenciaService agencia) {
        this.agencia = agencia;
    }
    
    @Override
    public int getRowCount() {
        return agencia.getLongitud();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }
    
    @Override
    public String getColumnName(int column){
        switch (column) {
            case 0: return "Mes";
            case 1: return "Agencia1";
            case 2: return "Agencia2";
            case 3: return "Agencia3";
            case 4: return "Agencia4";
            case 5: return "Agencia5";
            default: return null;
        }
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: return agencia.obtenerDato(0, rowIndex).getMes();
            case 1: return agencia.obtenerDato(0, rowIndex).getVenta();
            case 2: return agencia.obtenerDato(1, rowIndex).getVenta();
            case 3: return agencia.obtenerDato(2, rowIndex).getVenta();
            case 4: return agencia.obtenerDato(3, rowIndex).getVenta();
            case 5: return agencia.obtenerDato(4, rowIndex).getVenta();
            
            default: return null;
        }    
    }
}
